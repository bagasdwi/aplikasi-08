package dwi.bagas.appx08

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val FONT_SUBTITLE = "subFont"
    val FONT_DETAIL = "F"
    val DEF_FONT_SIZE = 20
    val DEF_TEXT = "Hello World"
    val bgHeader = "header"
    val bgMain = "background"
    val DEF_BACK = "White"
    val TITLE_FILM = "The Amazing Spiderman"
    val DEFF_FILM = "MARVEL ENTERTAINMENT"
    val CONTENT_FILM = "Content Film"
    val DEFF_CONTENT_FILM = "Sebuah cairan hitam aneh dari dunia lain melekat ke tubuh Peter Parker dan menyebabkan kekacauan batinnya. Pada saat bersamaan, muncul sosok penjahat baru yang mengancam warga New York - Sandman."

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.mnSetting -> {
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun getBackground(myString: String, el:Boolean){
        if(myString.equals("Green")){
            if (el){
                bgAct.setBackgroundColor(Color.GREEN)
                txtTitle.setTextColor(Color.BLACK)
                txtSubTitle.setTextColor(Color.BLACK)
                txtDetail.setTextColor(Color.BLACK)
                ln1.setBackgroundColor(Color.BLACK)
                ln2.setBackgroundColor(Color.BLACK)
            }else{
                bgH.setBackgroundColor(Color.GREEN)
                txtHeader.setTextColor(Color.BLACK)
            }
        }
        if(myString.equals("Blue")){
            if (el){
                bgAct.setBackgroundColor(Color.BLUE)
                txtTitle.setTextColor(Color.WHITE)
                txtSubTitle.setTextColor(Color.WHITE)
                txtDetail.setTextColor(Color.WHITE)
                ln1.setBackgroundColor(Color.WHITE)
                ln2.setBackgroundColor(Color.WHITE)
            }else{
                bgH.setBackgroundColor(Color.BLUE)
                txtHeader.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Yellow")){
            if (el){
                bgAct.setBackgroundColor(Color.YELLOW)
                txtTitle.setTextColor(Color.BLACK)
                txtSubTitle.setTextColor(Color.BLACK)
                txtDetail.setTextColor(Color.BLACK)
                ln1.setBackgroundColor(Color.BLACK)
                ln2.setBackgroundColor(Color.BLACK)
            }else{
                bgH.setBackgroundColor(Color.YELLOW)
                txtHeader.setTextColor(Color.BLACK)
            }
        }
        if(myString.equals("Black")){
            if (el){
                bgAct.setBackgroundColor(Color.BLACK)
                txtTitle.setTextColor(Color.WHITE)
                txtSubTitle.setTextColor(Color.WHITE)
                txtDetail.setTextColor(Color.WHITE)
                ln1.setBackgroundColor(Color.WHITE)
                ln2.setBackgroundColor(Color.WHITE)
            }else{
                bgH.setBackgroundColor(Color.BLACK)
                txtHeader.setTextColor(Color.WHITE)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val wholeBack = preferences.getString(bgMain,DEF_BACK).toString()
        getBackground(wholeBack,true)
        val bgHd = preferences.getString(bgHeader,DEF_BACK).toString()
        getBackground(bgHd,false)
        txtTitle.setText(preferences.getString(TITLE_FILM,DEFF_FILM))
        txtSubTitle.setTextSize(preferences.getInt(FONT_SUBTITLE,DEF_FONT_SIZE).toFloat())
        txtDetail.setText(preferences.getString(CONTENT_FILM,DEFF_CONTENT_FILM))
        txtDetail.setTextSize(preferences.getInt(FONT_DETAIL,DEF_FONT_SIZE).toFloat())
        Toast.makeText(this,"Perubahan telah disimpan.",Toast.LENGTH_SHORT).show()
    }
}