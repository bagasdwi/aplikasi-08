package dwi.bagas.appx08

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*


class SettingActivity: AppCompatActivity(), View.OnClickListener {

    var background: String = ""
    var selectedBack : String = ""
    var currentBack = ""
    var checkHD = ""
    var getTitle = ""

    lateinit var adapterSpin : ArrayAdapter<String>
    val arrayBackground = arrayOf("Blue","Yellow","Green","Black")

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    //    spinner
    val bgMain = "background"
    val DEF_BACK = "White"
    val bgHeader = "header"
    var checkedRadio = ""
    val TITLE_FILM = "The Amazing Spiderman"
    val CONTENT_FILM = "Content Film"
    val DEFF_CONTENT_FILM = "Sebuah cairan hitam aneh dari dunia lain melekat ke tubuh Peter Parker dan menyebabkan kekacauan batinnya. Pada saat bersamaan, muncul sosok penjahat baru yang mengancam warga New York - Sandman."
    val DEFF_FILM = "MARVEL ENTERTAINMENT"
    val FONT_SUBTITLE = "subFont"
    val DEF_FONT_SIZE = 20
    val FONT_DETAIL = "F"

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putString(bgMain,selectedBack)
        prefEditor.putString(bgHeader,checkedRadio)
        prefEditor.putString(TITLE_FILM,txtTitle.text.toString())
        prefEditor.putInt(FONT_SUBTITLE,seekBar2.progress)
        prefEditor.putString(CONTENT_FILM,txtContent.text.toString())
        prefEditor.putInt(FONT_DETAIL,seekBar3.progress)
        prefEditor.commit()
        System.exit(0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        currentBack = preferences.getString(bgMain,DEF_BACK).toString()
        adapterSpin = ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayBackground)
        checkHD = preferences.getString(bgHeader,DEF_BACK).toString()
        txtTitle.setText(preferences.getString(TITLE_FILM,DEFF_FILM))
        txtContent.setText(preferences.getString(CONTENT_FILM,DEFF_CONTENT_FILM))
        seekBar2.progress = preferences.getInt(FONT_SUBTITLE,DEF_FONT_SIZE)
        seekBar3.progress = preferences.getInt(FONT_DETAIL,DEF_FONT_SIZE)
        spBackground.adapter = adapterSpin
        spBackground.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                background = adapterSpin.getItem(position).toString()
                if(background.equals("Blue")){
                    selectedBack = "Blue"
                }
                if(background.equals("Yellow")){
                    selectedBack = "Yellow"
                }
                if(background.equals("Green")){
                    selectedBack = "Green"
                }
                if(background.equals("Black")){
                    selectedBack = "Black"
                }
            }
        }

        radioGroup.setOnCheckedChangeListener{group, checkedId ->
            when(checkedId){
                R.id.rbBlue->{
                    checkedRadio = "Blue"
                }R.id.rbYellow->{
                checkedRadio = "Yellow"
            }R.id.rbGreen->{
                checkedRadio = "Green"
            }R.id.rbBlack->{
                checkedRadio = "Black"
            }
            }
        }

        btnSimpan.setOnClickListener(this)
    }



    override fun onStart() {
        super.onStart()
        spBackground.setSelection(getIndex(spBackground,currentBack))
        getChecked()
    }

    fun getIndex(spinner: Spinner, myString: String) : Int {
        var a = spinner.count
        var b : String =""
        for(i in 0 until a){
            b = arrayBackground.get(i)
            if(b.equals(myString,ignoreCase = true)){
                return  i
            }
        }
        return 0
    }

    fun getChecked(){
        if(checkHD.equals("Blue")){
            rbBlue.isChecked = true
        }
        if(checkHD.equals("Green")){
            rbGreen.isChecked = true
        }
        if(checkHD.equals("Yellow")){
            rbYellow.isChecked = true
        }
        if(checkHD.equals("Black")){
            rbBlack.isChecked = true
        }
    }
}